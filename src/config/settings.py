import os
from pathlib import Path

import environ

env = environ.Env(
    SECRET_KEY=(str, ""),
    JWT_SECRET_KEY=(str, ""),
    ACCESS_TOKEN_EXPIRATION_TIME_IN_MINUTES=(int, 0),
    REFRESH_TOKEN_EXPIRATION_TIME_IN_MINUTES=(int, 0),
    PASSWORD_SALT=(str, ""),
    ALLOWED_HOSTS=(list, []),
    TELEGRAM_BOT_TOKEN=(str, ""),
    TELEGRAM_BOT_WEBHOOK_URL=(str, ""),
    POSTGRES_DB=(str, ""),
    POSTGRES_USER=(str, ""),
    POSTGRES_PASSWORD=(str, ""),
    POSTGRES_HOST=(str, ""),
    CSRF_TRUSTED_ORIGIN=(str, ""),
    POSTGRES_PORT=(str, ""),
    DEBUG=(bool, False),
)

BASE_DIR = Path(__file__).resolve().parent.parent
env.read_env(os.path.join(os.path.dirname(BASE_DIR), ".env"))

SECRET_KEY = env("SECRET_KEY")
JWT_SECRET_KEY = env("JWT_SECRET_KEY")
ACCESS_TOKEN_EXPIRATION_TIME_IN_MINUTES = env("ACCESS_TOKEN_EXPIRATION_TIME_IN_MINUTES")
REFRESH_TOKEN_EXPIRATION_TIME_IN_MINUTES = env("REFRESH_TOKEN_EXPIRATION_TIME_IN_MINUTES")
PASSWORD_SALT = env("PASSWORD_SALT")
TELEGRAM_BOT_TOKEN = env("TELEGRAM_BOT_TOKEN")
TELEGRAM_BOT_WEBHOOK_URL = env("TELEGRAM_BOT_WEBHOOK_URL")
CSRF_TRUSTED_ORIGINS = env.list("CSRF_TRUSTED_ORIGIN", default=[])
ALLOWED_HOSTS = env.list("ALLOWED_HOSTS")
DEBUG = env("DEBUG")

PASSWORD_HASHERS = ("django.contrib.auth.hashers.MD5PasswordHasher",)

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "environ",
    "rest_framework",
    "app",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "config.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "config.wsgi.application"

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": env("POSTGRES_DB"),
        "USER": env("POSTGRES_USER"),
        "PASSWORD": env("POSTGRES_PASSWORD"),
        "HOST": env("POSTGRES_HOST"),
        "PORT": env("POSTGRES_PORT"),
    }
}


AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": [
        "app.internal.transport.rest.auth_handlers.HTTPJWTAuth",
    ]
}


LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = True

USE_TZ = True

STATIC_URL = "/static/"
STATIC_ROOT = "/app/src/static/"


DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"
AUTH_USER_MODEL = "app.AdminUser"
