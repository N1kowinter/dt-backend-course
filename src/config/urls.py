from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

from app.internal.urls import create_api

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/", create_api().urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
