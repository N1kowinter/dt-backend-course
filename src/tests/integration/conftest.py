import pytest

from app.internal.models.bank_account import BankAccount
from app.internal.models.bot_user import BotUser
from app.internal.models.card import Card


@pytest.fixture
def create_bot_user_without_phone():
    user = BotUser(id=1, username="test1", first_name="test1", last_name="test1")
    user.save()
    return user


@pytest.fixture
def create_bot_user1():
    user = BotUser(id=2, username="test2", first_name="test2", last_name="test2", phone_number="+1234567890")
    user.save()
    return user


@pytest.fixture
def create_bot_user2():
    user = BotUser(id=3, username="test3", first_name="test3", last_name="test3", phone_number="+1234567899")
    user.save()
    return user


@pytest.fixture
def create_bot_user3():
    user = BotUser(id=4, username="test4", first_name="test4", last_name="test4", phone_number="+1234567892")
    user.save()
    return user


@pytest.fixture
def create_bank_account1(create_bot_user1):
    account = BankAccount(user=create_bot_user1)
    account.save()
    return account


@pytest.fixture
def create_bank_account2(create_bot_user2):
    account = BankAccount(user=create_bot_user2)
    account.save()
    return account


@pytest.fixture
def create_card1(create_bank_account1):
    card = Card(
        card_number="1234567890123456", bank_account=create_bank_account1, expiration_date="2028-12-12", cvv="123"
    )
    card.save()
    return card


@pytest.fixture
def create_card2(create_bank_account2):
    card = Card(
        card_number="1234567890123457", bank_account=create_bank_account2, expiration_date="2028-12-12", cvv="123"
    )
    card.save()
    return card
