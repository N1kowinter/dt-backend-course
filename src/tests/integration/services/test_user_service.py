import pytest

from app.internal.services.user_service import get_user_by_card_number, get_user_by_id, get_user_by_username, save_user


@pytest.mark.django_db
def test_get_user(create_bot_user1):
    user = get_user_by_id(create_bot_user1.id)
    assert user == create_bot_user1


@pytest.mark.django_db
def test_get_user_by_card_number(create_card1):
    user = get_user_by_card_number(create_card1.card_number)
    assert user == create_card1.bank_account.user


@pytest.mark.django_db
def test_get_user_by_username(create_bot_user1):
    user = get_user_by_username(create_bot_user1.username)
    assert user == create_bot_user1


@pytest.mark.django_db
def test_save_user(create_bot_user_without_phone):
    user = get_user_by_id(create_bot_user_without_phone.id)
    user.phone_number = "+1234567890"
    save_user(user)
    user = get_user_by_id(create_bot_user_without_phone.id)
    assert user.phone_number == "+1234567890"
