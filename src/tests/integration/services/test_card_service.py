import pytest

from app.internal.services.card_service import (
    get_card_by_card_number,
    get_card_by_id,
    get_cards_by_bank_account_id,
    save_card,
)


@pytest.mark.django_db
def test_get_card_by_id(create_card1):
    card = get_card_by_id(create_card1.id)
    assert card == create_card1


@pytest.mark.django_db
def test_get_card_by_number(create_card1):
    card = get_card_by_card_number(create_card1.card_number)
    assert card == create_card1


@pytest.mark.django_db
def test_get_cards_by_bank_account_id(create_card1):
    cards = get_cards_by_bank_account_id(create_card1.bank_account.id)
    assert cards == [create_card1]


@pytest.mark.django_db
def test_save_card(create_card1):
    card = get_card_by_id(create_card1.id)
    card.money = 100
    save_card(card)
    card = get_card_by_id(create_card1.id)
    assert card.money == 100
