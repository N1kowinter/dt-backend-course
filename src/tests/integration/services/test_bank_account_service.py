import pytest

from app.internal.models.bank_account import BankAccount
from app.internal.models.bot_user import BotUser
from app.internal.models.card import Card
from app.internal.services.bank_account_service import (
    delete_favorite_by_user_id,
    get_bank_account_by_id,
    get_bank_account_by_user_id,
    get_favorites_by_bank_account_id,
    save_bank_account,
    save_favorite,
)
from app.internal.services.user_service import get_user_by_id


@pytest.mark.django_db
def test_get_bank_account_without_bank_account_id(create_bot_user1):
    bank_account = get_bank_account_by_id(create_bot_user1.id)
    assert bank_account is None


@pytest.mark.django_db
def test_get_bank_account_by_id(create_bank_account1):
    bank_account = get_bank_account_by_id(create_bank_account1.id)
    assert bank_account == create_bank_account1


@pytest.mark.django_db
def test_get_bank_account_by_user_id(create_bank_account1):
    bank_account = get_bank_account_by_user_id(create_bank_account1.user.id)
    assert bank_account == create_bank_account1


@pytest.mark.django_db
def test_get_favorites(create_bank_account1, create_bank_account2):
    user2 = get_user_by_id(3)
    create_bank_account1.favorites.add(user2)
    assert get_favorites_by_bank_account_id(create_bank_account1.id) == [user2]


@pytest.mark.django_db
def test_save_bank_account(create_bank_account1, create_bank_account2):
    user1 = get_user_by_id(2)
    user2 = get_user_by_id(3)
    save_favorite(user2, user1.id)
    save_bank_account(create_bank_account1)
    bank_account = get_bank_account_by_id(create_bank_account1.id)
    assert get_favorites_by_bank_account_id(bank_account.id) == [user2]


@pytest.mark.django_db
def test_save_favorite(create_bank_account1, create_bank_account2):
    user1 = get_user_by_id(2)
    user2 = get_user_by_id(3)
    save_favorite(user2, user1.id)
    favorites = get_favorites_by_bank_account_id(create_bank_account1.id)
    assert favorites == [user2]


@pytest.mark.django_db
def test_delete_favorite_by_user_id(create_bank_account1, create_bot_user2):
    save_favorite(create_bot_user2, create_bank_account1.user.id)
    delete_favorite_by_user_id(create_bank_account1.user.id, create_bot_user2.id)
    favorites = get_favorites_by_bank_account_id(create_bank_account1.id)
    assert favorites == []
