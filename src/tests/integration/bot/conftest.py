from unittest import mock

import pytest
from django.utils import timezone

from app.internal.models.bank_account import BankAccount
from app.internal.models.bot_user import BotUser
from app.internal.models.card import Card
from app.internal.services.bank_account_service import async_save_bank_account, async_save_favorite
from app.internal.services.card_service import async_save_card
from app.internal.services.user_service import async_save_user


@pytest.fixture
def update(text="", telegram_id=1, username="", first_name="", last_name=""):
    mocked_update = mock.AsyncMock()
    mocked_update.message.text = text
    mocked_update.message.from_user.id = telegram_id
    mocked_update.message.from_user.username = username
    mocked_update.message.from_user.first_name = first_name
    mocked_update.message.from_user.last_name = last_name
    yield mocked_update


@pytest.fixture
def context(user_data=None):
    if user_data is None:
        user_data = {}
    mocked_context = mock.MagicMock()
    mocked_context.user.data.__setitem__.side_effect = user_data.__setitem__
    mocked_context.user.data.__getitem__.side_effect = user_data.__getitem__
    return mocked_context


@pytest.fixture
async def create_bot_user_with_phone1():
    user = BotUser(
        id=6,
        username="test6",
        first_name="test6",
        last_name="test6",
        phone_number="+1234567893",
        created_at=timezone.now(),
    )
    await async_save_user(user)
    return user


@pytest.fixture
async def create_bot_user_with_phone_and_bank_account():
    user = BotUser(
        id=10,
        username="test10",
        first_name="test10",
        last_name="test10",
        phone_number="+1234567894",
        created_at=timezone.now(),
    )
    bank_account = BankAccount(user=user)
    await async_save_user(user)
    await async_save_bank_account(bank_account)
    return user


@pytest.fixture
async def create_bot_user_without_phone1():
    user = BotUser(id=7, username="test7", first_name="test7", last_name="test7", created_at=timezone.now())
    await async_save_user(user)
    return user


@pytest.fixture
async def create_bot_user_with_card1():
    user = BotUser(
        id=8,
        username="test8",
        first_name="test8",
        last_name="test8",
        phone_number="+1234567895",
        created_at=timezone.now(),
    )
    bank_account = BankAccount(user=user)
    await async_save_user(user)
    await async_save_bank_account(bank_account)
    card = Card(
        card_number="1234567890123451",
        bank_account=bank_account,
        expiration_date="2028-12-23",
        cvv="123",
        created_at=timezone.now(),
        money=1000,
    )
    await async_save_card(card)
    return user


@pytest.fixture
async def create_bot_user_with_card2():
    user = BotUser(
        id=9,
        username="test9",
        first_name="test9",
        last_name="test9",
        phone_number="+1234567985",
        created_at=timezone.now(),
    )
    bank_account = BankAccount(user=user)
    await async_save_user(user)
    await async_save_bank_account(bank_account)
    card = Card(
        card_number="1234567890123111",
        bank_account=bank_account,
        expiration_date="2028-12-23",
        cvv="123",
        created_at=timezone.now(),
        money=1000,
    )
    await async_save_card(card)
    return user


@pytest.fixture
async def create_bot_user_with_card3():
    user = BotUser(
        id=11,
        username="test11",
        first_name="test11",
        last_name="test11",
        phone_number="+1234567986",
        created_at=timezone.now(),
    )
    bank_account = BankAccount(user=user)
    await async_save_user(user)
    await async_save_bank_account(bank_account)
    card = Card(
        card_number="1234567890123112",
        bank_account=bank_account,
        expiration_date="2028-12-23",
        cvv="123",
        created_at=timezone.now(),
        money=1000,
    )
    await async_save_card(card)
    return user


@pytest.fixture
async def create_bot_user_with_card4():
    user = BotUser(
        id=12,
        username="test12",
        first_name="test12",
        last_name="test12",
        phone_number="+1234567987",
        created_at=timezone.now(),
    )
    bank_account = BankAccount(user=user)
    await async_save_user(user)
    await async_save_bank_account(bank_account)
    card = Card(
        card_number="1234567890123113",
        bank_account=bank_account,
        expiration_date="2028-12-23",
        cvv="123",
        created_at=timezone.now(),
        money=1000,
    )
    await async_save_card(card)
    return user


@pytest.fixture
async def create_bot_user_with_card5():
    user = BotUser(
        id=13,
        username="test13",
        first_name="test13",
        last_name="test13",
        phone_number="+1234567988",
        created_at=timezone.now(),
    )
    bank_account = BankAccount(user=user)
    await async_save_user(user)
    await async_save_bank_account(bank_account)
    card = Card(
        card_number="1234567890123114",
        bank_account=bank_account,
        expiration_date="2028-12-23",
        cvv="123",
        created_at=timezone.now(),
        money=1000,
    )
    await async_save_card(card)
    return user


@pytest.fixture
async def create_bot_user_with_card6():
    user = BotUser(
        id=14,
        username="test14",
        first_name="test14",
        last_name="test14",
        phone_number="+1234567989",
        created_at=timezone.now(),
    )
    bank_account = BankAccount(user=user)
    await async_save_user(user)
    await async_save_bank_account(bank_account)
    card = Card(
        card_number="1234567890123115",
        bank_account=bank_account,
        expiration_date="2028-12-23",
        cvv="123",
        created_at=timezone.now(),
        money=1000,
    )
    await async_save_card(card)
    return user


@pytest.fixture
async def create_bot_user_with_card7():
    user = BotUser(
        id=15,
        username="test15",
        first_name="test15",
        last_name="test15",
        phone_number="+1234567990",
        created_at=timezone.now(),
    )
    bank_account = BankAccount(user=user)
    await async_save_user(user)
    await async_save_bank_account(bank_account)
    card = Card(
        card_number="1234567890123116",
        bank_account=bank_account,
        expiration_date="2028-12-23",
        cvv="123",
        created_at=timezone.now(),
        money=1000,
    )
    await async_save_card(card)
    return user


@pytest.fixture
async def create_bot_user_with_favorite():
    user = BotUser(
        id=16,
        username="test16",
        first_name="test16",
        last_name="test16",
        phone_number="+1234567991",
        created_at=timezone.now(),
    )
    tempUser = BotUser(
        id=17,
        username="test17",
        first_name="test17",
        last_name="test17",
        phone_number="+1234567992",
        created_at=timezone.now(),
    )
    bank_account = BankAccount(user=user)
    await async_save_user(user)
    await async_save_user(tempUser)
    await async_save_bank_account(bank_account)
    await async_save_favorite(tempUser, user.id)
    return user
