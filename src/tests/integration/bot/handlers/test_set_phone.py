import pytest
from django.utils import timezone
from telegram.ext import ConversationHandler

from app.internal.models.bot_user import BotUser
from app.internal.services.user_service import async_get_user_by_id, async_save_user
from app.internal.transport.bot.handlers import SetPhoneState, set_ask_phone_number


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_set_phone(update, context, create_bot_user_with_phone1):
    user = await create_bot_user_with_phone1
    update.message.from_user.id = user.id
    update.message.from_user.username = user.username
    update.message.from_user.first_name = user.first_name
    update.message.from_user.last_name = user.last_name
    assert user.phone_number == "+1234567893"
    assert await set_ask_phone_number(update, context) == SetPhoneState.ASK_PHONE_NUMBER


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_set_phone_without_user(update, context):
    assert await set_ask_phone_number(update, context) == ConversationHandler.END
