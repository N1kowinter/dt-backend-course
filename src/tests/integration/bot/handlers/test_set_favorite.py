import pytest
from telegram.ext import ConversationHandler

from app.internal.services.bank_account_service import (
    async_get_bank_account_by_user_id,
    async_get_favorites_by_bank_account_id,
)
from app.internal.services.card_service import async_get_cards_by_bank_account_id
from app.internal.transport.bot.handlers import SetFavoritesState, set_ask_favorite, set_favorite


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_set_favorite_by_card_info(update, context, create_bot_user_with_card4, create_bot_user_with_card3):
    user = await create_bot_user_with_card3
    temp_user = await create_bot_user_with_card4
    update.message.from_user.id = user.id
    update.message.from_user.username = user.username
    update.message.from_user.first_name = user.first_name
    update.message.from_user.last_name = user.last_name
    temp_bank_account = await async_get_bank_account_by_user_id(temp_user.id)
    card = await async_get_cards_by_bank_account_id(temp_bank_account.id)
    card_number = card[0].card_number
    bank_account = await async_get_bank_account_by_user_id(user.id)
    update.message.text = card_number
    assert await async_get_favorites_by_bank_account_id(bank_account.id) == []
    await set_favorite(update, context)
    assert len(await async_get_favorites_by_bank_account_id(bank_account.id)) == 1
    assert await set_favorite(update, context) == ConversationHandler.END


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_set_favorite_by_username(update, context, create_bot_user_with_card5, create_bot_user_with_card6):
    user = await create_bot_user_with_card5
    temp_user = await create_bot_user_with_card6
    update.message.from_user.id = user.id
    update.message.from_user.username = user.username
    update.message.from_user.first_name = user.first_name
    update.message.from_user.last_name = user.last_name
    bank_account = await async_get_bank_account_by_user_id(user.id)
    update.message.text = temp_user.username
    assert await async_get_favorites_by_bank_account_id(bank_account.id) == []
    await set_favorite(update, context)
    assert len(await async_get_favorites_by_bank_account_id(bank_account.id)) == 1
    assert await set_favorite(update, context) == ConversationHandler.END


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_set_favorite_with_not_exist_user(update, context, create_bot_user_with_card7):
    user = await create_bot_user_with_card7
    update.message.from_user.id = user.id
    update.message.from_user.username = user.username
    update.message.from_user.first_name = user.first_name
    update.message.from_user.last_name = user.last_name
    bank_account = await async_get_bank_account_by_user_id(user.id)
    update.message.text = "not_exist_user"
    assert await async_get_favorites_by_bank_account_id(bank_account.id) == []
    assert await set_favorite(update, context) == SetFavoritesState.ASK_FAVORITES
    assert await async_get_favorites_by_bank_account_id(bank_account.id) == []
