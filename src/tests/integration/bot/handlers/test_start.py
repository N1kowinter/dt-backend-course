import pytest

from app.internal.services.user_service import async_get_user_by_id
from app.internal.transport.bot.handlers import start


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_start(update, context):
    telegram_id = 1234
    username = "username4"
    first_name = "first_name4"
    last_name = "last_name4"
    update.message.from_user.id = telegram_id
    update.message.from_user.username = username
    update.message.from_user.first_name = first_name
    update.message.from_user.last_name = last_name
    await start(update, context)
    user = await async_get_user_by_id(update.message.from_user.id)
    assert (
        user.id == telegram_id
        and user.username == username
        and user.first_name == first_name
        and user.last_name == last_name
    )
