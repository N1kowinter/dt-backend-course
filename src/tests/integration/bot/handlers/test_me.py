import pytest

from app.internal.services.user_service import get_user_by_id
from app.internal.transport.bot.handlers import me


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_me(update, context):
    await me(update, context)
