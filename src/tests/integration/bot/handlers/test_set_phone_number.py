import pytest
from telegram.ext import ConversationHandler

from app.internal.transport.bot.handlers import SetPhoneState, set_phone_number


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_set_phone_number(update, context, create_bot_user_without_phone1):
    update.message.text = "+1234567899"
    user = await create_bot_user_without_phone1
    update.message.from_user.id = user.id
    update.message.from_user.username = user.username
    update.message.from_user.first_name = user.first_name
    update.message.from_user.last_name = user.last_name

    assert await set_phone_number(update, context) == ConversationHandler.END


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_wrong_phone_number(update, context, create_bot_user_without_phone1):
    update.message.text = "1234567899"
    user = await create_bot_user_without_phone1
    update.message.from_user.id = user.id
    update.message.from_user.username = user.username
    update.message.from_user.first_name = user.first_name
    update.message.from_user.last_name = user.last_name

    assert await set_phone_number(update, context) == SetPhoneState.ASK_PHONE_NUMBER
