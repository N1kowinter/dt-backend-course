import pytest

from app.internal.services.bank_account_service import async_get_bank_account_by_user_id
from app.internal.services.card_service import async_get_cards_by_bank_account_id
from app.internal.transport.bot.handlers import SetPhoneState, create_card


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_create_card_with_phone(update, context, create_bot_user_with_phone_and_bank_account):
    user = await create_bot_user_with_phone_and_bank_account
    update.message.from_user.id = user.id
    update.message.from_user.username = user.username
    update.message.from_user.first_name = user.first_name
    update.message.from_user.last_name = user.last_name
    bank_account = await async_get_bank_account_by_user_id(user.id)
    cards = await async_get_cards_by_bank_account_id(bank_account.id)
    assert len(cards) == 0
    await create_card(update, context)
    assert len(await async_get_cards_by_bank_account_id(bank_account.id)) == 1


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_create_card_without_phone(update, context, create_bot_user_without_phone1):
    user = await create_bot_user_without_phone1
    update.message.from_user.id = user.id
    update.message.from_user.username = user.username
    update.message.from_user.first_name = user.first_name
    update.message.from_user.last_name = user.last_name
    assert await create_card(update, context) == -2


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_create_card_without_user(update, context):
    assert await create_card(update, context) == -1
