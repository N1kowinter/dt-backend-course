import json

import pytest
from telegram.ext import ConversationHandler

from app.internal.services.bank_account_service import (
    async_get_bank_account_by_user_id,
    async_get_favorites_by_bank_account_id,
)
from app.internal.transport.bot.handlers import DeleteFavoriteState, delete_favorite, delete_favorites_button


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_delete_favorite(update, context, create_bot_user_with_favorite):
    user = await create_bot_user_with_favorite
    update.message.from_user.id = user.id
    update.message.from_user.username = user.username
    update.message.from_user.first_name = user.first_name
    update.message.from_user.last_name = user.last_name

    bank_account = await async_get_bank_account_by_user_id(user.id)
    list_favorites = await async_get_favorites_by_bank_account_id(bank_account.id)
    data = {
        "favorite_id": list_favorites[0].id,
        "user_id": user.id,
    }
    update.callback_query.data = json.dumps(data)

    assert len(list_favorites) == 1
    assert await delete_favorite(update, context) == DeleteFavoriteState.DELETE_FAVORITE
    assert await delete_favorites_button(update, context) == ConversationHandler.END
    assert len(await async_get_favorites_by_bank_account_id(bank_account.id)) == 0
