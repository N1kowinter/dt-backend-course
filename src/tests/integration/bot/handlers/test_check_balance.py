import pytest
from telegram.ext import ConversationHandler

from app.internal.services.bank_account_service import async_get_bank_account_by_user_id
from app.internal.services.card_service import async_get_cards_by_bank_account_id
from app.internal.transport.bot.handlers import SetBalanceState, card_button, check_balance


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_check_balance(update, context, create_bot_user_with_card1):
    user = await create_bot_user_with_card1
    update.message.from_user.id = user.id
    update.message.from_user.username = user.username
    update.message.from_user.first_name = user.first_name
    update.message.from_user.last_name = user.last_name

    assert await check_balance(update, context) == SetBalanceState.ASK_CARD


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_card_button(update, context, create_bot_user_with_card2):
    user = await create_bot_user_with_card2
    update.message.from_user.id = user.id
    update.message.from_user.username = user.username
    update.message.from_user.first_name = user.first_name
    update.message.from_user.last_name = user.last_name

    bank_account = await async_get_bank_account_by_user_id(user.id)
    card = await async_get_cards_by_bank_account_id(bank_account.id)

    update.callback_query.data = card[0].id
    assert (await card_button(update, context)) == ConversationHandler.END
