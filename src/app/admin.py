from django.contrib import admin

from app.internal.models.bank_account import BankAccount
from app.internal.models.bot_user import BotUser
from app.internal.models.card import Card
from app.internal.models.issued_token import IssuedToken
from app.internal.models.money_transaction import MoneyTransaction

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"


admin.site.register(BotUser)
admin.site.register(BankAccount)
admin.site.register(Card)
admin.site.register(MoneyTransaction)
admin.site.register(IssuedToken)
