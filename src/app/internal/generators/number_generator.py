import random


def generate_number(length: int) -> str:
    card_number = []
    for _ in range(length):
        card_number.append(str(random.randint(0, 9)))
    return "".join(card_number)
