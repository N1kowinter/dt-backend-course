import datetime


def generate_expiration_date(gap: int) -> datetime.date:
    today = datetime.date.today()
    expiration_date = today.replace(year=today.year + gap)
    return expiration_date
