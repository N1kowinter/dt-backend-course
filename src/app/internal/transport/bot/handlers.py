import json
from decimal import Decimal
from enum import IntEnum

from django.core.exceptions import ObjectDoesNotExist, ValidationError
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackContext, CommandHandler, ContextTypes, ConversationHandler, Updater

from app.internal.generators.expiration_date_generator import generate_expiration_date
from app.internal.generators.number_generator import generate_number
from app.internal.locales.messages import (
    ask_amount_message,
    not_enough_money_message,
    send_balance_message,
    send_cancel_message,
    send_card_required_message,
    send_card_statement_message,
    send_failure_create_card_message,
    send_favorite_user_message,
    send_favorites_message,
    send_interacted_users_message,
    send_me_message,
    send_money_method_message,
    send_no_card_transactions_message,
    send_no_interacted_users_message,
    send_password_success,
    send_password_wrong_format,
    send_phone_number_required_message,
    send_select_card_message,
    send_select_favorite_for_delete_message,
    send_select_favorite_message,
    send_select_user_to_pay_card_message,
    send_set_ask_favorite_message,
    send_set_ask_password_message,
    send_set_ask_phone_number_message,
    send_set_favorite_message,
    send_set_phone_number_message,
    send_set_user_info_message,
    send_success_create_card_message,
    send_success_delete_favorite_message,
    send_success_payment_message,
    send_success_start_message,
    send_tokens_message,
    send_unknown_message,
    send_user_dont_exist_message,
    send_write_card_message,
    send_write_username_message,
    send_zero_favorites_message,
)
from app.internal.models.bank_account import BankAccount
from app.internal.models.bot_user import BotUser
from app.internal.models.card import Card
from app.internal.services.bank_account_service import (
    async_delete_favorite_by_user_id,
    async_does_user_have_bank_account,
    async_get_bank_account_by_user_id,
    async_get_favorites_by_bank_account_id,
    async_save_bank_account,
    async_save_favorite,
)
from app.internal.services.card_service import (
    async_get_card_by_card_number,
    async_get_card_by_id,
    async_get_cards_by_bank_account_id,
    async_save_card,
)
from app.internal.services.issued_token_service import generate_new_token_pair
from app.internal.services.money_transaction_service import (
    async_get_all_money_transactions_of_card,
    async_get_all_usernames_which_user_interacted_with,
    async_send_money,
    get_all_usernames_which_user_interacted_with,
)
from app.internal.services.user_service import (
    async_does_user_have_phone_number,
    async_does_user_with_card_number_exist,
    async_does_user_with_user_id_exist,
    async_does_user_with_username_exist,
    async_get_user_by_card_number,
    async_get_user_by_id,
    async_get_user_by_username,
    async_get_user_password,
    async_save_user,
    async_update_user_password,
)
from app.internal.transport.bot.keyboards import cards_keyboard, favorites_keyboard
from app.internal.validators import card_validator
from app.internal.validators.password_validator import does_password_match_format
from app.internal.validators.phone_validator import phone_validator


class Limits(IntEnum):
    MAX_CARDS = 3
    CARD_LENGTH = 16
    CVV_LENGTH = 3
    NUMBER_OF_GROUPS = 4


class SetPhoneState(IntEnum):
    ASK_PHONE_NUMBER = 1


class SetFavoritesState(IntEnum):
    ASK_FAVORITES = 1


class SetBalanceState(IntEnum):
    ASK_CARD = 1


class DeleteFavoriteState(IntEnum):
    DELETE_FAVORITE = 1


class CheckFavoritesState(IntEnum):
    CHECK_FAVORITES = 1


class SetPasswordState(IntEnum):
    READ_PASSWORD = 1


class SendMoneyState(IntEnum):
    ASK_METHOD = 1
    BY_CARD = 2
    ASK_FAVORITE_CARD = 3
    BY_USERNAME = 4
    ASK_USER_CARD = 5
    ASK_AMOUNT = 6
    BUILD_PAYMENT = 7


class CardStatementState(IntEnum):
    ASK_CARD = 1


def phone_number_required(command: CallbackContext) -> CallbackContext:
    async def wrapper(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
        try:
            if not await async_does_user_with_user_id_exist(update.message.from_user.id):
                await set_user_info(update, context)
                return -1

            if not await async_does_user_have_phone_number(update.message.from_user.id):
                await send_phone_number_required_message(update)
                return -2
        except AttributeError:
            if not await async_does_user_with_user_id_exist(update.callback_query.from_user.id):
                await set_user_info(update, context)
                return -1

            if not await async_does_user_have_phone_number(update.callback_query.from_user.id):
                await send_phone_number_required_message(update)
                return -2

        return await command(update, context)

    return wrapper


def card_required(command: CallbackContext) -> CallbackContext:
    async def wrapper(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
        if not await async_does_user_have_bank_account(update.message.from_user.id):
            await send_card_required_message(update)
            return

        return await command(update, context)

    return wrapper


async def start(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    if not await async_does_user_with_user_id_exist(update.message.from_user.id):
        user = BotUser(
            id=update.message.from_user.id,
            username=update.message.from_user.username,
            first_name=update.message.from_user.first_name,
            last_name=update.message.from_user.last_name,
        )
        await async_save_user(user)

    await send_success_start_message(update)


@phone_number_required
async def me(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    user = await async_get_user_by_id(update.message.from_user.id)

    await send_me_message(update, user.username)


async def cancel(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_cancel_message(update)
    return ConversationHandler.END


async def unknown(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_unknown_message(update)


async def set_ask_phone_number(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    if not await async_does_user_with_user_id_exist(update.message.from_user.id):
        await set_user_info(update, context)
        return ConversationHandler.END

    await send_set_ask_phone_number_message(update)

    return SetPhoneState.ASK_PHONE_NUMBER


async def set_user_info(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_set_user_info_message(update)


async def set_phone_number(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    phone_number = update.message.text
    try:
        phone_validator(str(phone_number))
    except ValidationError as e:
        await update.message.reply_text(e.message)
        return SetPhoneState.ASK_PHONE_NUMBER

    user = await async_get_user_by_id(update.message.from_user.id)
    user.phone_number = phone_number

    await async_save_user(user)

    await send_set_phone_number_message(update)
    return ConversationHandler.END


@phone_number_required
async def set_ask_password(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_set_ask_password_message(update)
    return SetPasswordState.READ_PASSWORD


@phone_number_required
async def set_read_password(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    if update.message is None:
        return SetPasswordState.READ_PASSWORD
    password = update.message.text

    if not does_password_match_format(password):
        await send_password_wrong_format(update)
        return SetPasswordState.READ_PASSWORD

    await async_update_user_password(update.message.from_user.id, password)
    await send_password_success(update)
    return ConversationHandler.END


@phone_number_required
async def login(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    user_id = update.message.from_user.id
    if await async_get_user_password(user_id) is None:
        await send_set_ask_password_message(update)
        return

    payload = generate_new_token_pair(user_id)
    await send_tokens_message(update, payload)


@phone_number_required
async def create_card(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    user = await async_get_user_by_id(update.message.from_user.id)

    if not (await async_does_user_have_bank_account(update.message.from_user.id)):
        bank_account = BankAccount(user=user)
        await async_save_bank_account(bank_account)
    else:
        bank_account = await async_get_bank_account_by_user_id(user.id)
    card_list = await async_get_cards_by_bank_account_id(bank_account.id)

    if len(card_list) >= Limits.MAX_CARDS:
        await send_failure_create_card_message(update, Limits.MAX_CARDS)
        return

    card = Card(
        card_number=generate_number(Limits.CARD_LENGTH),
        bank_account=bank_account,
        expiration_date=generate_expiration_date(Limits.NUMBER_OF_GROUPS),
        cvv=generate_number(Limits.CVV_LENGTH),
    )
    await async_save_card(card)

    await send_success_create_card_message(update)


@phone_number_required
@card_required
async def check_balance(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    user = await async_get_user_by_id(update.message.from_user.id)
    bank_account = await async_get_bank_account_by_user_id(user.id)
    cards = await async_get_cards_by_bank_account_id(bank_account.id)

    reply_markup = cards_keyboard(cards)

    await send_select_card_message(update, reply_markup=reply_markup)

    return SetBalanceState.ASK_CARD


@phone_number_required
async def set_ask_favorite(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_set_ask_favorite_message(update)

    return SetFavoritesState.ASK_FAVORITES


@phone_number_required
async def set_favorite(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    card_info = update.message.text
    user = None
    if await async_does_user_with_card_number_exist(card_info):
        user = await async_get_user_by_card_number(card_info)

    if await async_does_user_with_username_exist(card_info):
        user = await async_get_user_by_username(card_info)

    if user is None:
        await send_user_dont_exist_message(update)
        return SetFavoritesState.ASK_FAVORITES

    await async_save_favorite(user, update.message.from_user.id)

    await send_set_favorite_message(update)
    return ConversationHandler.END


@phone_number_required
async def delete_favorite(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    user = await async_get_user_by_id(update.message.from_user.id)
    bank_account = await async_get_bank_account_by_user_id(user.id)
    favorites = await async_get_favorites_by_bank_account_id(bank_account.id)

    reply_markup = favorites_keyboard(favorites, update.message.from_user.id)

    await send_select_favorite_for_delete_message(update, reply_markup=reply_markup)

    return DeleteFavoriteState.DELETE_FAVORITE


async def show_favorite(update: Updater, user_id: int, context: ContextTypes.DEFAULT_TYPE) -> None:
    bank_account = await async_get_bank_account_by_user_id(user_id)
    favorites = await async_get_favorites_by_bank_account_id(bank_account.id)

    if len(favorites) == 0:
        await send_zero_favorites_message(update)
        return ConversationHandler.END

    reply_markup = favorites_keyboard(favorites, user_id)

    await send_favorites_message(update, reply_markup=reply_markup)


@phone_number_required
async def check_favorite(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    await show_favorite(update, update.message.from_user.id, context)

    return CheckFavoritesState.CHECK_FAVORITES


@phone_number_required
async def delete_favorites_button(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    query = update.callback_query
    data = json.loads(query.data)
    favorite_user = await async_get_user_by_id(data["favorite_id"])
    await query.answer()

    await async_delete_favorite_by_user_id(data["user_id"], data["favorite_id"])

    await send_success_delete_favorite_message(query, favorite_user.username)

    return ConversationHandler.END


@phone_number_required
@card_required
async def ask_payment_method(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_money_method_message(update)

    return SendMoneyState.ASK_METHOD


async def show_card(update: Updater, user_id: int, context: ContextTypes.DEFAULT_TYPE) -> None:
    bank_account = await async_get_bank_account_by_user_id(user_id)
    cards = await async_get_cards_by_bank_account_id(bank_account.id)

    reply_markup = cards_keyboard(cards)
    await send_select_card_message(update, reply_markup=reply_markup)


async def payment_to_card(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    card_number = update.message.text
    user = await async_get_user_by_id(update.message.from_user.id)
    try:
        card_validator.card_number_validator(str(card_number))
    except ValidationError as e:
        await update.message.reply_text(e.message)
        return SendMoneyState.BY_CARD
    context.user_data["card_number_to_pay"] = card_number

    await show_card(update, user, context)

    return SendMoneyState.ASK_AMOUNT


async def show_user_to_pay_cards(update: Updater, bank_account_id: int, context: ContextTypes.DEFAULT_TYPE) -> None:
    cards = await async_get_cards_by_bank_account_id(bank_account_id)
    reply_markup = cards_keyboard(cards)
    await send_select_user_to_pay_card_message(update, reply_markup=reply_markup)


async def payment_to_username(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    user_to_pay = update.message.text
    if not async_does_user_with_username_exist(user_to_pay):
        await send_user_dont_exist_message(update)
        return SendMoneyState.BY_USERNAME

    user_to_pay = await async_get_user_by_username(user_to_pay)

    context.user_data["user_to_pay"] = user_to_pay
    user_to_pay_bank_account = await async_get_bank_account_by_user_id(user_to_pay.id)

    await show_user_to_pay_cards(update, user_to_pay_bank_account.id, context)

    return SendMoneyState.ASK_USER_CARD


async def select_card(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    query = update.callback_query
    favorite_card = await async_get_card_by_id(query.data)
    await query.answer()

    context.user_data["card_number_to_pay"] = favorite_card.card_number
    user = await async_get_user_by_id(query.from_user.id)

    await show_card(update, user, context)

    return SendMoneyState.ASK_AMOUNT


async def ask_amount(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    query = update.callback_query
    user_card = await async_get_card_by_id(query.data)
    await query.answer()
    context.user_data["user_card"] = user_card

    await ask_amount_message(query, user_card.money, user_card.currency)
    return SendMoneyState.BUILD_PAYMENT


async def build_payment(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    amount = update.message.text
    user_card = context.user_data["user_card"]
    amount = Decimal(amount)
    if user_card.money < amount:
        await not_enough_money_message(update)
        await ask_amount_message(update, user_card.money, user_card.currency)
        return SendMoneyState.BUILD_PAYMENT

    card_to_pay = await async_get_card_by_card_number(context.user_data["card_number_to_pay"])
    await async_send_money(user_card, card_to_pay, amount)

    await send_success_payment_message(update, amount, user_card.currency)

    return ConversationHandler.END


async def get_interacted_users(update: Updater, context: CallbackContext) -> None:
    user_id = update.message.from_user.id
    interacted_users = await async_get_all_usernames_which_user_interacted_with(user_id)
    if len(interacted_users) != 0:
        await send_interacted_users_message(update, "\n".join(interacted_users))
    else:
        await send_no_interacted_users_message(update)


@phone_number_required
@card_required
async def ask_card_for_statement(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    user = await async_get_user_by_id(update.message.from_user.id)
    bank_account = await async_get_bank_account_by_user_id(user.id)
    cards = await async_get_cards_by_bank_account_id(bank_account.id)

    reply_markup = cards_keyboard(cards)

    await send_select_card_message(update, reply_markup=reply_markup)

    return CardStatementState.ASK_CARD


async def card_statement(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    query = update.callback_query
    card = await async_get_card_by_id(query.data)
    await query.answer()

    transactions = await async_get_all_money_transactions_of_card(card.id)
    currency = card.currency
    transactions_text = []
    for date, sender_account_id, receiver_account_id, money_amount in transactions:
        sender_account_card_number = (await async_get_card_by_id(sender_account_id)).card_number
        receiver_account_card_number = (await async_get_card_by_id(receiver_account_id)).card_number
        transactions_text.append(
            f"{date.strftime('%Y-%m-%d %H:%M:%S')} {sender_account_card_number} {receiver_account_card_number} {money_amount} {currency}"
        )

    if len(transactions) == 0:
        await send_no_card_transactions_message(query)
    else:
        await send_card_statement_message(query, "\n".join(transactions_text))

    return ConversationHandler.END


async def card_button(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    query = update.callback_query
    card = await async_get_card_by_id(query.data)
    await query.answer()

    formatted_card_number = " ".join(
        [
            card.card_number[i : i + Limits.NUMBER_OF_GROUPS]
            for i in range(0, len(card.card_number), Limits.NUMBER_OF_GROUPS)
        ]
    )

    await send_balance_message(query, formatted_card_number, card)

    return ConversationHandler.END


async def check_favorites_button(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    query = update.callback_query
    data = json.loads(query.data)
    favorite_user = await async_get_user_by_id(data["favorite_id"])
    favorite_user_bank_account = await async_get_bank_account_by_user_id(data["favorite_id"])
    cards = await async_get_cards_by_bank_account_id(favorite_user_bank_account.id)
    await query.answer()

    await send_favorite_user_message(query, favorite_user.username, cards)

    return ConversationHandler.END


async def payment_method_button(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    query = update.callback_query
    method = query.data
    context.user_data["payment_method"] = method
    await query.answer()

    if method == "Card":
        await send_write_card_message(query)
        return SendMoneyState.BY_CARD
    elif method == "Favorites":
        await show_favorite(update, query.from_user.id, context)
        return SendMoneyState.ASK_FAVORITE_CARD
    else:
        await send_write_username_message(query)
        return SendMoneyState.BY_USERNAME


async def select_favorite_card_button(update: Updater, context: ContextTypes.DEFAULT_TYPE) -> None:
    query = update.callback_query
    data = json.loads(query.data)
    favorite_user_bank_account = await async_get_bank_account_by_user_id(data["favorite_id"])
    await query.answer()

    await show_user_to_pay_cards(update, favorite_user_bank_account.id, context)

    return SendMoneyState.ASK_USER_CARD
