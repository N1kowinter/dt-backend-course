import json

from telegram import InlineKeyboardButton, InlineKeyboardMarkup

from app.internal.models.bot_user import BotUser


def cards_keyboard(cards) -> InlineKeyboardMarkup:
    keyboard = [[InlineKeyboardButton(card.card_number, callback_data=str(card.id)) for card in cards]]
    return InlineKeyboardMarkup(keyboard)


def favorites_keyboard(favorites: list[BotUser], user_id: int) -> InlineKeyboardMarkup:
    keyboard = [
        [
            InlineKeyboardButton(
                favorite.username, callback_data=json.dumps({"user_id": user_id, "favorite_id": favorite.id})
            )
            for favorite in favorites
        ]
    ]
    return InlineKeyboardMarkup(keyboard)


def payment_methods_keyboard(payment_methods: list[str]) -> InlineKeyboardMarkup:
    keyboard = [[InlineKeyboardButton(method, callback_data=method) for method in payment_methods]]
    return InlineKeyboardMarkup(keyboard)
