from abc import ABC

import jwt
from django.conf import settings
from django.http import HttpRequest
from ninja.security import HttpBasicAuth, HttpBearer

from app.internal.services.jwt_service import decode_jwt_token


class HTTPJWTAuth(HttpBearer):
    def authenticate(self, request: HttpRequest, token):
        try:
            payload = decode_jwt_token(token)
            assert payload["type"] == "access"
            request.user_id = payload["id"]
        except Exception as e:
            print(e)
            return None
        return token
