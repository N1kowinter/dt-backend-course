from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from ninja import Router

from app.internal.services.issued_token_service import generate_new_token_pair
from app.internal.services.user_service import check_user_password

router = Router()


@router.post("/")
@csrf_exempt
def login(request, telegram_id: int, password: str):
    if check_user_password(telegram_id, password):
        return JsonResponse(generate_new_token_pair(telegram_id))
    return HttpResponse(content="Unauthorized", status=401)
