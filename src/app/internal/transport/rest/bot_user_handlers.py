from django.http import HttpResponse, HttpResponseNotFound, JsonResponse
from ninja import Router
from rest_framework import serializers

from app.internal.locales.messages import send_bot_user_not_found_message
from app.internal.models.bot_user import BotUser
from app.internal.services.user_service import get_user_by_id
from app.internal.transport.middlewares.auth_middlewares import HTTPJWTAuth


class BotUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = BotUser
        fields = ["first_name", "last_name", "username", "phone_number"]


router = Router(auth=HTTPJWTAuth())


@router.get("/{telegram_id}")
def get_bot_user(request, telegram_id: int):
    if telegram_id != request.user_id:
        return HttpResponse("Unauthorized", status=401)
    user = get_user_by_id(user_id=telegram_id)
    return (
        JsonResponse(BotUserSerializer(user).data)
        if user
        else HttpResponseNotFound(send_bot_user_not_found_message(telegram_id))
    )
