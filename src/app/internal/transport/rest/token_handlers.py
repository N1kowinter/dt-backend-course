from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from ninja import Router

from app.internal.services.issued_token_service import (
    generate_new_token_pair,
    is_refresh_token_exist,
    is_refresh_token_revoked,
    revoke_all_refresh_tokens,
)
from app.internal.services.jwt_service import decode_jwt_token

router = Router()


@router.post("/update")
@csrf_exempt
def update_tokens(request, refresh_token: str):
    if not is_refresh_token_exist(refresh_token):
        return HttpResponse(content="Given refresh token doesn't exist.", status=404)
    payload = decode_jwt_token(refresh_token)

    if is_refresh_token_revoked(refresh_token):
        revoke_all_refresh_tokens(payload["id"])
        return HttpResponse(content="Given refresh token is revoked. Log in to get a new one.", status=403)
    return JsonResponse(generate_new_token_pair(payload["id"]))
