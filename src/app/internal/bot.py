from warnings import filterwarnings

from telegram.ext import (
    Application,
    CallbackQueryHandler,
    CommandHandler,
    ConversationHandler,
    MessageHandler,
    Updater,
    filters,
)
from telegram.warnings import PTBUserWarning

from app.internal.transport.bot.handlers import (
    CardStatementState,
    CheckFavoritesState,
    DeleteFavoriteState,
    SendMoneyState,
    SetBalanceState,
    SetFavoritesState,
    SetPasswordState,
    SetPhoneState,
    ask_amount,
    ask_card_for_statement,
    ask_payment_method,
    build_payment,
    cancel,
    card_button,
    card_statement,
    check_balance,
    check_favorite,
    check_favorites_button,
    create_card,
    delete_favorite,
    delete_favorites_button,
    get_interacted_users,
    me,
    payment_method_button,
    payment_to_card,
    payment_to_username,
    select_card,
    select_favorite_card_button,
    set_ask_favorite,
    set_ask_password,
    set_ask_phone_number,
    set_favorite,
    set_phone_number,
    set_read_password,
    start,
    unknown,
)


def run(token: str, webhook_url: str) -> None:
    application = Application.builder().token(token).build()
    filterwarnings(action="ignore", message=r".*CallbackQueryHandler", category=PTBUserWarning)

    application.add_handler(CommandHandler("start", start))
    application.add_handler(CommandHandler("me", me))
    application.add_handler(
        ConversationHandler(
            entry_points=[CommandHandler("set_password", set_ask_password)],
            states={
                SetPasswordState.READ_PASSWORD: [
                    MessageHandler(filters=filters.TEXT & ~filters.COMMAND, callback=set_read_password)
                ],
            },
            fallbacks=[CommandHandler("cancel", cancel)],
        )
    )

    application.add_handler(
        ConversationHandler(
            entry_points=[CommandHandler("set_phone", set_ask_phone_number)],
            states={
                SetPhoneState.ASK_PHONE_NUMBER: [
                    MessageHandler(filters=filters.TEXT & ~filters.COMMAND, callback=set_phone_number)
                ]
            },
            fallbacks=[CommandHandler("cancel", cancel)],
        )
    )
    application.add_handler(CommandHandler("interacted_users", get_interacted_users))
    application.add_handler(CommandHandler("create_card", create_card))
    application.add_handler(
        ConversationHandler(
            entry_points=[CommandHandler("balance", check_balance)],
            states={SetBalanceState.ASK_CARD: [CallbackQueryHandler(card_button)]},
            fallbacks=[CommandHandler("cancel", cancel)],
            per_message=False,
        )
    )
    application.add_handler(
        ConversationHandler(
            entry_points=[CommandHandler("set_favorite", set_ask_favorite)],
            states={
                SetFavoritesState.ASK_FAVORITES: [
                    MessageHandler(filters=filters.TEXT & ~filters.COMMAND, callback=set_favorite)
                ]
            },
            fallbacks=[CommandHandler("cancel", cancel)],
        )
    )
    application.add_handler(
        ConversationHandler(
            entry_points=[CommandHandler("check_favorite", check_favorite)],
            states={CheckFavoritesState.CHECK_FAVORITES: [CallbackQueryHandler(check_favorites_button)]},
            fallbacks=[CommandHandler("cancel", cancel)],
            per_message=False,
        )
    )
    application.add_handler(
        ConversationHandler(
            entry_points=[CommandHandler("delete_favorite", delete_favorite)],
            states={DeleteFavoriteState.DELETE_FAVORITE: [CallbackQueryHandler(delete_favorites_button)]},
            fallbacks=[CommandHandler("cancel", cancel)],
            per_message=False,
        )
    )
    application.add_handler(
        ConversationHandler(
            entry_points=[CommandHandler("send", ask_payment_method)],
            states={
                SendMoneyState.ASK_METHOD: [CallbackQueryHandler(payment_method_button)],
                SendMoneyState.BY_CARD: [
                    MessageHandler(filters=filters.TEXT & ~filters.COMMAND, callback=payment_to_card)
                ],
                SendMoneyState.ASK_FAVORITE_CARD: [CallbackQueryHandler(select_favorite_card_button)],
                SendMoneyState.BY_USERNAME: [
                    MessageHandler(filters=filters.TEXT & ~filters.COMMAND, callback=payment_to_username)
                ],
                SendMoneyState.ASK_USER_CARD: [CallbackQueryHandler(select_card)],
                SendMoneyState.ASK_AMOUNT: [CallbackQueryHandler(ask_amount)],
                SendMoneyState.BUILD_PAYMENT: [
                    MessageHandler(filters=filters.TEXT & ~filters.COMMAND, callback=build_payment)
                ],
            },
            fallbacks=[CommandHandler("cancel", cancel)],
            per_message=False,
        )
    )
    application.add_handler(
        ConversationHandler(
            entry_points=[CommandHandler("card_statement", ask_card_for_statement)],
            states={CardStatementState.ASK_CARD: [CallbackQueryHandler(card_statement)]},
            fallbacks=[CommandHandler("cancel", cancel)],
            per_message=False,
        )
    )
    application.add_handler(MessageHandler(filters.COMMAND, unknown))

    application.run_webhook(listen="0.0.0.0", port=8443, webhook_url=webhook_url)
