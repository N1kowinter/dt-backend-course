from django.contrib import admin

from app.internal.models.bot_user import BotUser


@admin.register(BotUser)
class BotUserAdmin(admin.ModelAdmin):
    fields = ("id", "username", "first_name", "last_name", "phone_number", "created_at")
    list_display = ("username", "first_name", "last_name", "phone_number")
