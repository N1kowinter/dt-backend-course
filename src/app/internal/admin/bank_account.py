from django.contrib import admin

from app.internal.models.bank_account import BankAccount


@admin.register(BankAccount)
class BankAccountAdmin(admin.ModelAdmin):
    field = ("id", "created_at", "updated_at")
    list_display = ("user", "card")
