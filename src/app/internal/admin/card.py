from django.contrib import admin

from app.internal.models.card import Card


@admin.register(Card)
class CardAdmin(admin.ModelAdmin):
    fields = ("id", "card_number", "expiration_date", "cvv", "created_at", "money")
    list_display = ("card_number", "expiration_date", "cvv", "money")
