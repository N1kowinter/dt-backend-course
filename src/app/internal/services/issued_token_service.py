from app.internal.models.issued_token import IssuedToken
from app.internal.services.hash_converters import convert_string_to_md5
from app.internal.services.jwt_service import generate_jwt_access_token, generate_jwt_refresh_token


def generate_refresh_token(id: int) -> str:
    token = generate_jwt_refresh_token(id)
    IssuedToken.objects.create(jwi=convert_string_to_md5(token), user_id=id)
    return token


def generate_new_token_pair(id: int) -> dict:
    revoke_all_refresh_tokens(id)
    return {
        "access_token": generate_jwt_access_token(id),
        "refresh_token": generate_refresh_token(id),
    }


def is_refresh_token_exist(token: str) -> bool:
    return IssuedToken.objects.filter(jwi=convert_string_to_md5(token)).exists()


def is_refresh_token_revoked(token: str) -> bool:
    return IssuedToken.objects.values_list("revoked", flat=True).get(jwi=convert_string_to_md5(token))


def revoke_all_refresh_tokens(id: int):
    IssuedToken.objects.filter(user_id=id).update(revoked=True)
