from asgiref.sync import sync_to_async

from app.internal.models.bank_account import BankAccount
from app.internal.models.bot_user import BotUser
from app.internal.models.card import Card


def get_bank_account_by_id(bank_account_id: int) -> BankAccount | None:
    try:
        return BankAccount.objects.get(id=bank_account_id)
    except BankAccount.DoesNotExist:
        return None


def get_bank_account_by_user_id(user_id: int) -> BankAccount | None:
    try:
        return BankAccount.objects.get(user=user_id)
    except BankAccount.DoesNotExist:
        return None


def save_bank_account(bank_account: BankAccount) -> None:
    bank_account.save()


def save_favorite(user_to_add, user_id) -> None:
    get_bank_account_by_user_id(user_id).favorites.add(user_to_add)


def get_favorites_by_bank_account_id(bank_account_id: int) -> list[BotUser]:
    try:
        return list(get_bank_account_by_id(bank_account_id).favorites.all())
    except BankAccount.DoesNotExist:
        return None


def delete_favorite_by_user_id(user_id: int, favorite_id: int) -> None:
    get_bank_account_by_user_id(user_id).favorites.remove(favorite_id)


def does_user_have_bank_account(user_id: int) -> bool:
    return BankAccount.objects.filter(user=user_id).exists()


@sync_to_async
def async_get_bank_account(bank_account_id: int) -> BankAccount | None:
    return get_bank_account_by_id(bank_account_id)


@sync_to_async
def async_get_bank_account_by_user_id(user_id: int) -> BankAccount | None:
    return get_bank_account_by_user_id(user_id)


@sync_to_async
def async_save_bank_account(bank_account: BankAccount) -> None:
    save_bank_account(bank_account)


@sync_to_async
def async_save_favorite(user_to_add: BotUser, user_id: int) -> None:
    save_favorite(user_to_add, user_id)


@sync_to_async
def async_get_favorites_by_bank_account_id(bank_account_id: int) -> list[BotUser]:
    return get_favorites_by_bank_account_id(bank_account_id)


@sync_to_async
def async_delete_favorite_by_user_id(user_id: int, favorite_id: int) -> None:
    delete_favorite_by_user_id(user_id, favorite_id)


@sync_to_async
def async_does_user_have_bank_account(user_id: int) -> bool:
    return does_user_have_bank_account(user_id)
