from asgiref.sync import sync_to_async
from django.conf import settings
from django.contrib.auth.hashers import check_password, make_password
from django.core.exceptions import ObjectDoesNotExist

from app.internal.models.bank_account import BankAccount
from app.internal.models.bot_user import BotUser
from app.internal.models.card import Card


def get_user_password(user_id: int) -> str:
    return BotUser.objects.values_list("password", flat=True).filter(id=user_id).first()


def update_user_password(user_id: int, password: str) -> None:
    BotUser.objects.filter(id=user_id).update(password=make_password(password, salt=settings.PASSWORD_SALT))


def check_user_password(user_id: int, password: str) -> bool:
    user_password = get_user_password(user_id)
    return user_password is not None and check_password(password, user_password)


def get_user_by_id(user_id: int) -> BotUser | None:
    try:
        return BotUser.objects.get(id=user_id)
    except BotUser.DoesNotExist:
        return None


def get_user_by_card_number(card_number: int) -> BotUser | None:
    try:
        bank_account = Card.objects.get(card_number=card_number).bank_account
        return bank_account.user
    except ObjectDoesNotExist:
        return None


def get_user_by_username(username: str) -> BotUser | None:
    try:
        return BotUser.objects.get(username=username)
    except BotUser.DoesNotExist:
        return None


def save_user(user: BotUser) -> None:
    user.save()


def does_user_with_user_id_exist(user_id: int) -> bool:
    return BotUser.objects.filter(id=user_id).exists()


def does_user_with_username_exist(username: str) -> bool:
    return BotUser.objects.filter(username=username).exists()


def does_user_have_phone_number(user_id: int) -> bool:
    phone_number = BotUser.objects.filter(id=user_id).values_list("phone_number").first()
    return phone_number is not None and phone_number[0] is not None


def does_user_with_card_number_exist(card_number: int) -> bool:
    return Card.objects.filter(card_number=card_number).exists()


@sync_to_async
def async_get_user_by_id(user_id: int) -> BotUser | None:
    return get_user_by_id(user_id)


@sync_to_async
def async_get_user_by_card_number(card_number: int) -> BotUser | None:
    return get_user_by_card_number(card_number)


@sync_to_async
def async_get_user_by_username(username: str) -> BotUser | None:
    return get_user_by_username(username)


@sync_to_async
def async_save_user(user: BotUser) -> None:
    save_user(user)


@sync_to_async
def async_does_user_with_user_id_exist(user_id: int) -> bool:
    return does_user_with_user_id_exist(user_id)


@sync_to_async
def async_does_user_with_username_exist(username: str) -> bool:
    return does_user_with_username_exist(username)


@sync_to_async
def async_does_user_have_phone_number(user_id: int) -> bool:
    return does_user_have_phone_number(user_id)


@sync_to_async
def async_does_user_with_card_number_exist(card_number: int) -> bool:
    return does_user_with_card_number_exist(card_number)


@sync_to_async
def async_get_user_password(user_id: int) -> str:
    return get_user_password(user_id)


@sync_to_async
def async_update_user_password(user_id: int, password: str) -> None:
    update_user_password(user_id, password)


@sync_to_async
def async_check_user_password(user_id: int, password: str) -> bool:
    return check_user_password(user_id, password)
