from datetime import datetime, timedelta

import jwt
from django.conf import settings


def decode_jwt_token(jwt_token: str) -> dict:
    return jwt.decode(jwt_token, settings.JWT_SECRET_KEY, algorithms=["HS256"])


def generate_jwt_access_token(user_id: int) -> str:
    return jwt.encode(
        {
            "id": user_id,
            "type": "access",
            "exp": datetime.now() + timedelta(minutes=settings.ACCESS_TOKEN_EXPIRATION_TIME_IN_MINUTES),
        },
        settings.JWT_SECRET_KEY,
        algorithm="HS256",
    )


def generate_jwt_refresh_token(id: int):
    return jwt.encode(
        {
            "id": id,
            "type": "refresh",
            "exp": datetime.now() + timedelta(minutes=settings.REFRESH_TOKEN_EXPIRATION_TIME_IN_MINUTES),
        },
        settings.JWT_SECRET_KEY,
        algorithm="HS256",
    )
