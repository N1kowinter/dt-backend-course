from asgiref.sync import sync_to_async

from app.internal.models.card import Card


def save_card(card) -> None:
    card.save()


def get_cards_by_bank_account_id(bank_account_id: int) -> list[Card] | None:
    try:
        return list(Card.objects.filter(bank_account__id=bank_account_id))
    except Card.DoesNotExist:
        return None


def get_card_by_id(card_id: int) -> Card | None:
    try:
        return Card.objects.get(id=card_id)
    except Card.DoesNotExist:
        return None


def get_card_by_card_number(card_number: str) -> Card | None:
    try:
        return Card.objects.get(card_number=card_number)
    except Card.DoesNotExist:
        return None


@sync_to_async
def async_save_card(card) -> None:
    save_card(card)


@sync_to_async
def async_get_cards_by_bank_account_id(bank_account_id: int):
    return get_cards_by_bank_account_id(bank_account_id)


@sync_to_async
def async_get_card_by_id(card_id: int):
    return get_card_by_id(card_id)


@sync_to_async
def async_get_card_by_card_number(card_number: str):
    return get_card_by_card_number(card_number)
