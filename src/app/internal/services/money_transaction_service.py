from asgiref.sync import sync_to_async
from django.db.models import F

from app.internal.models.card import Card
from app.internal.models.money_transaction import MoneyTransaction


def send_money(sender_account_card: Card, receiver_account_card: Card, money_amount: float) -> None:
    Card.objects.filter(id=sender_account_card.id).update(money=F("money") - money_amount)
    Card.objects.filter(id=receiver_account_card.id).update(money=F("money") + money_amount)
    MoneyTransaction.objects.create(
        sender_account=sender_account_card, receiver_account=receiver_account_card, money_amount=money_amount
    )


def get_all_usernames_which_user_interacted_with(user_id: int) -> list[str]:
    return list(
        MoneyTransaction.objects.filter(sender_account__bank_account__user__id=user_id)
        .values_list("receiver_account__bank_account__user__username", flat=True)
        .union(
            MoneyTransaction.objects.filter(receiver_account__bank_account__user_id=user_id).values_list(
                "sender_account__bank_account__user__username", flat=True
            )
        )
    )


def get_all_money_transactions_of_card(card_id: int) -> list[tuple]:
    return [
        (date, sender_account_id, receiver_account_id, money_amount)
        for date, sender_account_id, receiver_account_id, money_amount, id in MoneyTransaction.objects.filter(
            sender_account__id=card_id
        )
        .union(MoneyTransaction.objects.filter(receiver_account__id=card_id))
        .values_list("date", "sender_account__id", "receiver_account__id", "money_amount", "id")
        .order_by("date")
    ]


@sync_to_async
def async_send_money(sender_account_id: int, receiver_account_id: int, money_amount: float) -> None:
    send_money(sender_account_id, receiver_account_id, money_amount)


@sync_to_async
def async_get_all_usernames_which_user_interacted_with(user_id: int) -> list[str]:
    return get_all_usernames_which_user_interacted_with(user_id)


@sync_to_async
def async_get_all_money_transactions_of_card(card_id: int) -> list[MoneyTransaction]:
    return get_all_money_transactions_of_card(card_id)
