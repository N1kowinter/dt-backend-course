from django.db import models

from app.internal.validators.phone_validator import phone_validator


class BotUser(models.Model):
    id = models.PositiveBigIntegerField(primary_key=True, null=False, blank=False, unique=True)
    username = models.CharField(max_length=255, null=False, blank=False, unique=True)
    first_name = models.CharField(max_length=255, null=False, blank=False)
    last_name = models.CharField(max_length=255, null=True, blank=True)
    phone_number = models.CharField(max_length=16, validators=[phone_validator], null=True, blank=True, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    password = models.CharField(max_length=255, null=False)
