from django.db import models

from app.internal.models.card import Card


class MoneyTransaction(models.Model):
    sender_account = models.ForeignKey(Card, on_delete=models.CASCADE, related_name="sender_account")
    receiver_account = models.ForeignKey(Card, on_delete=models.CASCADE, related_name="receiver_account")
    money_amount = models.DecimalField(max_digits=10, decimal_places=2)
    date = models.DateTimeField(auto_now_add=True)
