import uuid

from django.db import models

from app.internal.models.bot_user import BotUser


class BankAccount(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.OneToOneField(BotUser, on_delete=models.CASCADE)
    favorites = models.ManyToManyField(BotUser, related_name="favorites")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
