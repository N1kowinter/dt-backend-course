import uuid

from django.db import models

from app.internal.models.bank_account import BankAccount
from app.internal.validators.card_validator import card_number_validator, cvv_validator


class Card(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    card_number = models.CharField(
        max_length=16, validators=[card_number_validator], null=False, blank=False, unique=True
    )
    bank_account = models.ForeignKey(BankAccount, on_delete=models.CASCADE)
    expiration_date = models.DateField(null=False, blank=False)
    cvv = models.CharField(max_length=3, validators=[cvv_validator], null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
    money = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    currency = models.CharField(max_length=3, default="USD")
