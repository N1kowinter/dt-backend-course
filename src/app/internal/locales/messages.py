from fluent.runtime import FluentLocalization, FluentResourceLoader
from telegram import CallbackQuery, InlineKeyboardMarkup
from telegram.ext import Updater

from app.internal.transport.bot.keyboards import payment_methods_keyboard

loader = FluentResourceLoader("src/app/internal/locales/{locale}")
en = FluentLocalization(["en"], ["main.ftl"], loader)


async def send_phone_number_required_message(update: Updater):
    await update.message.reply_text(en.format_value("phone_number_required"))


async def send_card_required_message(update: Updater):
    await update.message.reply_text(en.format_value("card_required"))


async def send_success_start_message(update: Updater):
    await update.message.reply_text(en.format_value("start_success"))


async def send_me_message(update: Updater, user):
    await update.message.reply_text(en.format_value("me", {"user": user}))


async def send_cancel_message(update: Updater):
    await update.message.reply_text(en.format_value("cancel"))


async def send_unknown_message(update: Updater):
    await update.message.reply_text(en.format_value("unknown"))


async def send_set_ask_phone_number_message(update: Updater):
    await update.message.reply_text(en.format_value("set_ask_phone_number"))


async def send_set_user_info_message(update: Updater):
    await update.message.reply_text(en.format_value("set_user_info"))


async def send_set_phone_number_message(update: Updater):
    await update.message.reply_text(en.format_value("set_phone_number"))


async def send_failure_create_card_message(update: Updater, MAX_CARDS: int):
    await update.message.reply_text(en.format_value("create_card_failure", {"MAX_CARDS": MAX_CARDS}))


async def send_success_create_card_message(update: Updater):
    await update.message.reply_text(en.format_value("create_card_success"))


async def send_select_card_message(update: Updater, reply_markup: InlineKeyboardMarkup):
    try:
        await update.message.reply_text(en.format_value("select_card"), reply_markup=reply_markup)
    except:
        await update.callback_query.message.reply_text(en.format_value("select_card"), reply_markup=reply_markup)


async def send_balance_message(query, formatted_card_number: str, card):
    await query.message.reply_text(
        en.format_value(
            "balance", {"card_number": formatted_card_number, "balance": card.money, "currency": card.currency}
        )
    )


async def send_set_ask_favorite_message(update: Updater):
    await update.message.reply_text(en.format_value("set_ask_favorite"))


async def send_user_dont_exist_message(update: Updater):
    await update.message.reply_text(en.format_value("user_dont_exist"))


async def send_set_favorite_message(update: Updater):
    await update.message.reply_text(en.format_value("set_favorite"))


async def send_select_favorite_for_delete_message(update: Updater, reply_markup: InlineKeyboardMarkup):
    await update.message.reply_text(en.format_value("select_favorite_for_delete"), reply_markup=reply_markup)


async def send_success_delete_favorite_message(query: CallbackQuery, favorite_user):
    await query.message.reply_text(en.format_value("delete_favorite_success", {"favorite_user": favorite_user}))


async def send_favorite_user_message(query: CallbackQuery, favorite_user, cards):
    cards_str = "\n".join(str(card) for card in cards)
    await query.message.reply_text(
        en.format_value("favorite_user", {"favorite_user": favorite_user, "cards": cards_str})
    )


async def send_zero_favorites_message(update: Updater):
    try:
        await update.message.reply_text(en.format_value("zero_favorites"))
    except AttributeError:
        await update.callback_query.message.reply_text(en.format_value("zero_favorites"))


async def send_favorites_message(update: Updater, reply_markup: InlineKeyboardMarkup):
    try:
        await update.message.reply_text(en.format_value("your_favorites"), reply_markup=reply_markup)
    except AttributeError:
        await update.callback_query.message.reply_text(en.format_value("your_favorites"), reply_markup=reply_markup)


async def send_money_method_message(update: Updater):
    payment_methods = en.format_value("payment_methods").split(", ")
    reply_markup = payment_methods_keyboard(payment_methods)

    await update.message.reply_text(en.format_value("choose_payment_methods"), reply_markup=reply_markup)


async def send_write_card_message(query: CallbackQuery):
    await query.message.reply_text(en.format_value("write_card"))


async def send_select_favorite_message(query: CallbackQuery):
    await query.message.reply_text(
        en.format_value("select_favorite"),
    )


async def send_write_username_message(query: CallbackQuery):
    await query.message.reply_text(en.format_value("write_username"))


async def ask_amount_message(query: CallbackQuery, balance: int, currency: str):
    await query.message.reply_text(en.format_value("ask_amount", {"balance": balance, "currency": currency}))


async def not_enough_money_message(update: Updater):
    await update.message.reply_text(en.format_value("not_enough_money"))


async def send_success_payment_message(update: Updater, amount: int, currency: str):
    await update.message.reply_text(en.format_value("success_payment", {"amount": amount, "currency": currency}))


async def send_set_ask_password_message(update: Updater):
    await update.message.reply_text(en.format_value("set_ask_password"))


async def send_password_wrong_format(update: Updater):
    await update.message.reply_text(en.format_value("password_wrong_format"))


async def send_password_success(update: Updater):
    await update.message.reply_text(en.format_value("password_success"))


async def send_tokens_message(update: Updater, payload: str):
    access_token = payload["access"]
    refresh_token = payload["refresh"]
    await update.message.reply_text(en.format_value("tokens", {"access": access_token, "refresh": refresh_token}))


async def send_select_user_to_pay_card_message(update: Updater, reply_markup: InlineKeyboardMarkup):
    try:
        await update.message.reply_text(en.format_value("select_user_to_pay_card"), reply_markup=reply_markup)
    except AttributeError:
        await update.callback_query.message.reply_text(
            en.format_value("select_user_to_pay_card"), reply_markup=reply_markup
        )


def send_validation_error_message(digit: int):
    return en.format_value("length_validation", {"digit": digit})


def send_bot_user_not_found_message(id: int):
    return en.format_value("bot_user_not_found", {"id": id})


async def send_no_interacted_users_message(update: Updater):
    try:
        await update.message.reply_text(en.format_value("no_interacted_users"))
    except AttributeError:
        await update.callback_query.message.reply_text(en.format_value("no_interacted_users"))


async def send_interacted_users_message(update: Updater, interacted_users: str):
    try:
        await update.message.reply_text(en.format_value("interacted_users", {"interacted_users": interacted_users}))
    except AttributeError:
        await update.callback_query.message.reply_text(
            en.format_value("interacted_users", {"interacted_users": interacted_users})
        )


async def send_no_card_transactions_message(update: Updater) -> None:
    try:
        await update.message.reply_text(en.format_value("no_card_transactions"))
    except AttributeError:
        await update.callback_query.message.reply_text(en.format_value("no_card_transactions"))


async def send_card_statement_message(update: Updater, transactions: str) -> None:
    try:
        await update.message.reply_text(en.format_value("card_statement", {"transactions": transactions}))
    except AttributeError:
        await update.callback_query.message.reply_text(
            en.format_value("card_statement", {"transactions": transactions})
        )
