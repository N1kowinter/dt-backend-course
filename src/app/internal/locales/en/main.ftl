phone_number_required = You should use the /set_phone command to set your phone number.
card_required = You should use the /create_card command to create a card.
me = Your information: { $user }
cancel = Operation canceled.
unknown = Sorry, I didn't understand that command.
set_ask_phone_number = Please enter your phone number:
create_card_failure = You can't create more than { $MAX_CARDS } cards.
create_card_success = Your card has been created successfully!
balance = Card number: { $card_number }
          Balance: { $balance } { $currency }
start_success = Your information has been saved successfully!
                You should use the /set_phone command to set your phone number.
set_user_info = There is no user information.
                You should use the /start command to set your information.
set_phone_number = Your phone number has been saved successfully!
                   You can create a card using the /create_card command.
length_validation = Length must be { $digit } digits
user_dont_exist = The user does not exist. 
                  Try to write correct username.
set_ask_favorite = Please enter the card number or username of the user you wish to add to your favorites:
set_favorite = The user associated with the provided card information has been successfully added to your favorites.
select_favorite_for_delete = Please choose the user you wish to remove from your favorites:
delete_favorite_success = The user { $favorite_user } has been successfully removed from your favorites.
zero_favorites = You don't have any favorites
favorite_user = Favorite User: 
                { $favorite_user }
                Cards: 
                { $cards }
your_favorites = Your favorites:
choose_payment_methods = Please choose a payment method from the following options:
payment_methods = Card, Favorites, Username
write_card = Please enter card details:
select_card = Please select your card:
select_favorite = Please select a favorite from the following options:
write_username = Please enter username:
ask_amount = Balance: { $balance } { $currency }
             Please enter the amount you wish to send:
not_enough_money = Sorry, you don't have enough funds to complete this transaction. Please enter a smaller amount.
success_payment = Congratulations! 
                  Your payment of { $amount } { $currency } has been successfully processed.
select_user_to_pay_card = Please choose your preferred card for sending money.
no_interacted_users = You haven't interacted with anyone yet.
interacted_users = Interacted Users:
                    { $interacted_users }
no_card_transactions = No transaction.
card_statement = Your transactions:
                    { $transactions }
set_ask_password = Enter a password with characters from 8 to 64 in count.
password_wrong_format = Given password doesn't match the format.
password_success = Password set successfully.
NO_PASSWORD = You have no password. Set a password with /set_password before logging in.
tokens = Access Token: { $access } 
          Refresh Token: { $refresh }
bot_user_not_found = The bot user with an telegram id { $id } hasn't been found