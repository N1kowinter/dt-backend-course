import re

from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator


def phone_validator(value: str) -> None:
    phone_regex = r"^\+\d{8,15}$"

    validation_error_message = "Phone number must starts with '+' and contain 8 to 15 digits."

    try:
        RegexValidator(regex=phone_regex)(value)
    except ValidationError:
        raise ValidationError(validation_error_message)
