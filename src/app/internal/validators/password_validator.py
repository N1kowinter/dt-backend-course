from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator


def phone_validator(value: str) -> None:
    password_regex = r"^[a-zA-z0-9]{8,64}$"

    validation_error_message = ("Password must be entered with latin characters or digits from 8 to 64 in count.",)

    try:
        RegexValidator(regex=password_regex)(value)
    except ValidationError:
        raise ValidationError(validation_error_message)


def does_password_match_format(password: str) -> bool:
    try:
        phone_validator(password)
    except ValidationError:
        return False
    return True
