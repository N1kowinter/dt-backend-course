from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator

from app.internal.locales.messages import send_validation_error_message


def card_number_validator(value: str) -> None:
    length_limit = 16
    card_number_regex = r"^\d{16}$"

    try:
        RegexValidator(regex=card_number_regex)(value)
    except ValidationError:
        raise ValidationError(send_validation_error_message(length_limit))


def cvv_validator(value: str) -> None:
    length_limit = 3
    cvv_regex = r"^\d{3}$"

    try:
        RegexValidator(regex=cvv_regex)(value)
    except ValidationError:
        raise ValidationError(send_validation_error_message(length_limit))
