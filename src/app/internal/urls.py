from ninja import NinjaAPI

from app.internal.transport.rest import bot_user_handlers, login_handlers, token_handlers


def create_api() -> NinjaAPI:
    api = NinjaAPI(version="1.0.0", csrf=True)
    api.add_router("/me/", bot_user_handlers.router)
    api.add_router("/login/", login_handlers.router)
    api.add_router("/token/", token_handlers.router)
    return api
