from django.core.management.base import BaseCommand

from app.internal.bot import run
from config.settings import TELEGRAM_BOT_TOKEN, TELEGRAM_BOT_WEBHOOK_URL


class Command(BaseCommand):
    def handle(self, *args, **options) -> None:
        run(TELEGRAM_BOT_TOKEN, TELEGRAM_BOT_WEBHOOK_URL)
