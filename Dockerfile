FROM python:3.11.3-slim-buster

WORKDIR /app

RUN apt update && apt-get install make

COPY pyproject.toml poetry.lock ./

RUN pip install --no-cache-dir poetry && \
    poetry config virtualenvs.create false && \
    poetry install --only main --no-interaction --no-root


COPY .env Makefile setup.cfg ./

COPY ./src ./src
