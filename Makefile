include .env


build:
	docker build -t ${APP_IMAGE} .
	docker build -t ${NGINX_IMAGE} nginx

up:
	make docker_migrate docker_up

down:
	docker-compose down

pull:
	docker pull ${APP_IMAGE}

push:
	docker push ${APP_IMAGE}

migrate:
	python src/manage.py migrate $(if $m, api $m,)

docker_migrate:
	docker-compose up -d db
	docker-compose run --rm web make migrate
	docker-compose stop db

docker_up:
	docker-compose up -d

makemigrations:
	python src/manage.py makemigrations

createsuperuser:
	python src/manage.py createsuperuser

collectstatic:
	python src/manage.py collectstatic --no-input

runserver:
	python src/manage.py runserver 0.0.0.0:8000

runbot:
	python src/manage.py runbot

dev:
	make migrate collectstatic run runbot

command:
	python src/manage.py ${c}

shell:
	python src/manage.py shell

debug:
	python src/manage.py debug

lock:
	poetry lock

test:
	pytest

ci_test:
	docker-compose up -d db
	docker-compose run --rm web make test
	docker-compose stop db

lint:
	isort .
	flake8 --config setup.cfg
	black --config pyproject.toml .

check_lint:
	isort --check --diff .
	flake8 --config setup.cfg
	black --check --config pyproject.toml .

ci_check_lint:
	docker container run --rm --name ${APP_CONTAINER} ${APP_IMAGE} make check_lint